#include "stdafx.h"

#include <time.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include<cstdlib>
#include<queue>

using namespace std;

const int MAP_SIZE = 120;
const int MAX_PATH_SIZE = (MAP_SIZE * 2) + 1;  
const int MAX_OPEN_SIZE = MAP_SIZE*2;
int cycles = 0;
int pointsChecked = 0;
int maxIndexSoFar = 1; //temporarily here. return as static to open_list() when done debugging


//////////////////////Temp defining obstacles section//////
const int obs1x = 25;
const int obs1xdelta = 22;
const int obs1y = 25;
const int obs1ydelta = 2;
const int obs2x = 20;
const int obs2xdelta = 10;
const int obs2y = 20;
const int obs2ydelta = 2;
//////////////END temp defining obstacles///////////////


struct node {//10cmx10cm grid squares irl
	int x = 0;
	int y = 0;
	int status = 0; //0 = go, 1 = temp no go, 2 = permanent no go, 3 = path, 4 = start, 5 = target
	int F = 9999999;
	int H = 999999; //straight line distance to target
	int G = 999999; // dykstra distance from start(current position)
	int prevX = 0; //x and y of node coming from
	int prevY = 0;
	//int parentOpenPos = 99;//open[i] position ...for removing from open list
	bool closed = false;
	bool open = false;
};
node map[MAP_SIZE][MAP_SIZE];
node path[MAX_PATH_SIZE+1];

struct point {//waypoints and open/closed lists
	int x;
	int y;
	bool slotFilled = false;
	int F = 9999999;
	int stepsLeft = MAX_PATH_SIZE;
};
point start;
point target;
point open[MAX_OPEN_SIZE];
point closed[MAP_SIZE*MAP_SIZE];

void print();
void FillMap();
void get_start_and_target();
void pathFind();
int open_list(point incoming, int task); //task  -  1 = open, 0 = close, 2 return index of point in open[] with lowest F val

class pathing {
public:
	void get_path(); //serout optimized path

//private:
	void optimize(); //make private later
	queue<node> myQueue;
};

int main()
{

	FillMap();//// For now fixed, later load from file
	
	get_start_and_target();//

	int stopwatch = time(NULL);
	
	pathFind();

		 /////build path and print path to screen////
	/************************** ADD THIS To shorten path list and elimiate unecessary zigzagging: ***************
	AFTER ROBOT STARTS ALONG PATH, start at target and work backwards - 
	calculate straight line as y=mx+b, then check for obstacles between target path[MAX] and previous points, 
	working backwards until obstacle found or we've reached next point robot will arrive at.
	Then remove intermediate point from path[] list/queue (and close up gap or mark for ignore). Repeat if stopping point was an obstacle and not
	the next point robot will arrive at.
	***************************************************************************************************************
	*/

	cout << endl << "<<<<<<<<<<>>>>>>>>>>>>>>" << endl;
	cout << endl << "<<<<<<<<<<>>>>>>>>>>>>>>" << endl;
	

	for (int i = MAX_PATH_SIZE; i>0 ; i--) {
		path[i - 1].x = map[path[i].x][path[i].y].prevX;
		path[i - 1].y = map[path[i].x][path[i].y].prevY;
		path[i].prevX = path[i - 1].x;
		path[i].prevY = path[i - 1].y;

		if(!(path[i - 1].x== path[0].x&&path[i - 1].y == path[0].y))//marks for cout output
		map[path[i - 1].x][path[i - 1].y].status = 3;

		
		if (path[i].x != 0 && path[i].y != 0) {
			cout << "step " << i << " - " << path[i].x << ", " << path[i].y << " <---------" << endl;
		}

	}
	pathing ourpath;
	ourpath.optimize();
	while (!ourpath.myQueue.empty()) {
		//int i = 0;
		if (ourpath.myQueue.front().x == 0 || ourpath.myQueue.front().y == 0) 
		{
			ourpath.myQueue.pop(); 
		}
		if(    (ourpath.myQueue.front().x !=start.x && ourpath.myQueue.front().y!=start.y)
			&& (ourpath.myQueue.front().x != target.x && ourpath.myQueue.front().y != target.y) ) 
		{
			map[ourpath.myQueue.front().x][ourpath.myQueue.front().y].status = 6; ///setting .status to actual waypoint
		}

		
		cout << "Queue size " << ourpath.myQueue.size() <<" - "<< ourpath.myQueue.front().x << ", " << ourpath.myQueue.front().y << endl;
		ourpath.myQueue.pop();
	}
	stopwatch = time(NULL) - stopwatch; //maybe remove when installed in robot
	
	print();
	cout << " Pathfinding took " << stopwatch << " seconds and  " << cycles 
		<< " cycles through path() while loop, and we checked a total of " <<pointsChecked<<" points" << endl;
	system("pause");
	return 0;
}


////////////////////////////START PATHFIND()////////////////////////////////
void pathFind() {
	/*OPEN[]//list of nodes to be checked
	CLOSED//nodes already checked (just set node.closed bool to true?

	LOOP{
	current= open w lowest f cost. if equal, go with lowest h val
	remove current from open
	place current in closed

	if(current == target){
	break}
	else{ //for every neighbor of current
	if point closed or in obstacle list skip;
	else 	calc f for every neighbor into temp ints

	if (new f < old F OR point not in open){
	set F val
	set prevx and prevy of this neighbor to "current" (aka"parent" node)
	if neighbor not in open - set to open

	buildpath(); // backtrack using parent node points and build path[] array with known number or steps
	store number pf steps left in path[] points,
	*/

	start.x = path[0].x;
	start.y = path[0].y;
	map[start.x][start.y].prevX = 0; //aka "parent" node
	map[start.x][start.y].prevY = 0;
	target.x = path[MAX_PATH_SIZE].x;
	target.y = path[MAX_PATH_SIZE].y;
	map[start.x][start.y].G = 0;
	point current;
	current.x = start.x;
	current.y = start.y;
	current.F = 999999;

	open[0] = current;
	open_list(current, 1);	//current = start; //initialize current as start

	while (map[current.x][current.y].H >= 1) 
	{
		cycles++;
	//	cout << "Path() while loop cyle # " << cycles;
	
		for (int i = 0; i <= maxIndexSoFar; i++) {

			if (open[i].slotFilled == 1) {
			}
		}
	
		
		if (cycles>1) {
			open_list(current, 0); //close existing current
			current = open[open_list(current, 2)];//just update to open point w lowest f value//this belongs later
		}
		for (int y = current.y - 1; y <= current.y + 1; y++) 
		{
			for (int x = current.x - 1; x <= current.x + 1; x++) 
			{
				
				if ((!map[x][y].closed) && !(x == current.x&&y == current.y)) 
				{
				if (map[x][y].H == 999999) 
					{
						cout << ".";
						int deltaX = 0;
						int deltaY = 0;
						if (x > target.x) deltaX = (x - target.x) * 10;
						else deltaX = (target.x - x) * 10;
						if (y > target.y) deltaY = (y - target.y) * 10;
						else deltaY = (target.y - y) * 10;
						map[x][y].H = sqrt(pow(deltaX, 2) + pow(deltaY, 2));
					
					}////////////////END get H value////////////////////////////////////

					////////////////check if new F is lower//////////////
					pointsChecked++;

					int tempG;
					int tempF;
					if (x == current.x || y == current.y) tempG = map[current.x][current.y].G + 10;
					else tempG = map[current.x][current.y].G + 14;
					tempF = tempG + map[x][y].H;
					
					if (tempF < map[x][y].F) {
						map[x][y].G = tempG;
						map[x][y].F = tempF;
						map[x][y].prevX = current.x;
						map[x][y].prevY = current.y;
					}
					if (map[x][y].open == false) { //add to open list if not already
						point temp;
						temp.x = x;
						temp.y = y;
						temp.F = map[x][y].F;
						open_list(temp, 1);
					}
				
				}

			}
		}
		
	}
}////////////////////////////END PATHFIND()////////////////////////////////
///////////////////////START optimize()///////////////////////

//////////////////////////START OPEN_LIST()///////////////////////
int open_list(point incoming, int task){ //task  -  1 = open, 0 = close, 2 = just return open[] point with lowest F
	
	///////////////////ADD function to update f value////////////////////////////////
	/////////////////////////////////////////////////////////////********************************


	//static int maxIndexSoFar = 1; temporarily making global. bring back here as static when done debugging
	int lowF = open[0].F;
	int lowF_i = 0;
	if (task == 0) {//removing from open[]
			for (int i = 0; i <= maxIndexSoFar; i++) {
				if (open[i].x == incoming.x&&open[i].y == incoming.y) {
				//	cout << "###########removing " << incoming.x << ", " << incoming.y << " from open list##########" << endl;
					open[i].x = 0;
					open[i].y = 0;
					open[i].F = 9999999;
					open[i].slotFilled = false;
					map[incoming.x][incoming.y].closed = true;
					map[incoming.x][incoming.y].open = false;
				//	break;
				}
			}
		
	}
	else if(task == 1){ //adding to open[]cout << "here with task 1 to add to open list incoming x,y, F val = "<< incoming.x<<", "<< incoming.y<<"  "<< incoming.F << endl;
		for (int i = 0; i < MAX_OPEN_SIZE;i++) {
		//for (int i = 0; i <= maxIndexSoFar+2;i++) {
			if (!map[incoming.x][incoming.y].open && !open[i].slotFilled) {
				map[incoming.x][incoming.y].open = true;
				open[i].x = incoming.x;
				open[i].y = incoming.y;
				open[i].F = incoming.F;
				open[i].slotFilled = true;
			}
			if (open[i].F < lowF) {
				lowF = open[i].F;
				lowF_i = i;
			}
			if (i > maxIndexSoFar) maxIndexSoFar=i;

		}
	}
	else if (task == 2) {
		//for (int i = 0; i < MAX_OPEN_SIZE; i++) {
		for (int i = 0; i <= maxIndexSoFar; i++) {
			if (open[i].F < lowF) {
				lowF = open[i].F;
				lowF_i = i;
			}
		}
	}

	//cout << "returning lowF index of " << lowF_i <<" point x,y = "<<open[lowF_i].x<<", "<< open[lowF_i].y<<"  F = "<< open[lowF_i].F<<  endl;
	return lowF_i;
}

///////////start get start point and target////////////////
void get_start_and_target() {

	while (map[path[0].x][path[0].y].status != 4) {
		cout << "enter x and y values from 0-" << MAP_SIZE-1 << " for start point" << endl;
		cin >> path[0].x >> path[0].y;
		if (map[path[0].x][path[0].y].status != 0) {
			cout << "ERROR, Input was inside an obstacle or outside of map. Try again" << endl;
		}
		else	map[path[0].x][path[0].y].status = 4;
	}
	while (map[path[MAX_PATH_SIZE].x][path[MAX_PATH_SIZE].y].status != 5) {
		cout << "enter x and y values from 0-" << MAP_SIZE - 1 << " for target point" << endl;
		cin >> path[MAX_PATH_SIZE].x >> path[MAX_PATH_SIZE].y;
		if (map[path[MAX_PATH_SIZE].x][path[MAX_PATH_SIZE].y].status != 0) {
			cout << "ERROR, Input was inside an obstacle or outside of map. Try again" << endl;
		}
		else 	map[path[MAX_PATH_SIZE].x][path[MAX_PATH_SIZE].y].status = 5;
	}
	//
	//target.x = path[MAX_PATH_SIZE].x;
	//target.y = path[MAX_PATH_SIZE].y;

	}

/////////////////////START PRINT () ///////////////////////////////
void print() {//0 = go, 1 = temp no go, 2 = permanent no go, 3 = path, 4 = start, 5 = target
	
	for (int y = MAP_SIZE; y >= 0; y--) {
		for (int x = 0; x < MAP_SIZE; x++) {
			if (map[x][y].status == 0) { cout << ' '; }
			if (map[x][y].status == 1) { cout << '#'; }
			if (map[x][y].status == 2) { cout << '#'; }
			if (map[x][y].status == 3) { cout << '.'; }
			if (map[x][y].status == 4) { cout << 'O'; }
			if (map[x][y].status == 5) { cout << 'X'; }
			if (map[x][y].status == 6) { cout << 'x'; }
		}cout << endl;
	}cout << "Legend:  O = start, X = target, # = obstruction, . = path "<<endl;


}

/////////////////////START FILLMAP () ///////////////////////////////
void FillMap() {
	//ofstream mapin;
	//ifstream mapin;
	//mapin.open("map.txt");
	//mapin << "This is the map file" << endl;
	//mapin.close();
	//////////temp///actual house layout  move to file eventually so robot can build its own/////////
	for (int y = 0; y < 97; y++)//east wall
	{
		map[73][y].closed = 1;
		map[73][y].status = 2;
	}
	for (int x = 42; x < 51; x++)//tstat wall
	{
		map[x][36].closed = 1;
		map[x][36].status = 2;
	}
	for(int x=60;x<73;x++) //extended tstat wall
	{
		map[x][36].closed = 1;
		map[x][36].status = 2;
	}
	for (int y = 36; y < 72; y++)//tv wall
	{
		map[73][y].closed = 1;
		map[73][y].status = 2;
	}
	for (int y = 81; y < 115; y++) //front door wall
	{
		map[73][y].closed = 1;
		map[73][y].status = 2;
	}









	///////////end actual house layout section//////////
	for (int y = 0; y<MAP_SIZE; y++) {//////edges
		for (int x = 0; x<MAP_SIZE; x++) {
			if ((y == 0 && x == 0)||(y == 0 && x != 0) ||(y != 0 && x == 0))				
			{
				map[x][y].status = 1;
				map[x][y].closed = true;
			}
			else if (
				 (y == MAP_SIZE - 1 && x == MAP_SIZE - 1)
				|| (y == MAP_SIZE - 1 && x != MAP_SIZE - 1)
				|| (y != MAP_SIZE - 1 && x == MAP_SIZE - 1)
				) 
			{
				map[x][y].status = 1;
				map[x][y].closed = true;
			}
		}
	}
	/*
	for (int y = obs1y; y<obs1y + obs1ydelta; y++) {//////obstacle 1
		for (int x = obs1x; x<obs1x + obs1xdelta; x++) {
			map[x][y].status = 1;
			map[x][y].closed = true;
		}
	}

	for (int y = obs2y; y<obs2y + obs2ydelta; y++) {//////obstacle 2
		for (int x = obs2x; x<obs2x + obs2xdelta; x++) {
			map[x][y].status = 1;
			map[x][y].closed = true;
		}

	}
	*/
}/////////////////////end fillmap()/////////////////////////////////

void pathing::optimize() {
	int x1, x2, y1, y2;
	int i = 0;
	while (path[i].x == 0 && path[i].y == 0 && i < MAX_PATH_SIZE) { i++; }
		x1 = path[i].x;
		y1 = path[i].y;
		x2 = path[i+2].x;
		y2 = path[i+2].y;
			
	//store y=mx+b line data here
	double m = 0;
	int b = 0;

	cout << path[0].x << ", " << path[0].y << endl;
	myQueue.push(path[0]);        // add first point no matter what
	if (path[1].x != 0 && path[1].y != 0) myQueue.push(path[1]);//add second point if not empty


	for (int i = 2; i <= MAX_PATH_SIZE; i++) { //find the longest straight lines to minimize slow zig zagging
		


		if (path[i].x != 0 && path[i].y != 0) {//trying to cut waypoints between straight lines
		
			if (i < MAX_PATH_SIZE &&
				(((path[i].x == path[i].prevX && path[i].x == path[i + 1].x) ||
				(path[i].y == path[i].prevY && path[i].y == path[i + 1].y)
				)) 
				|| ((path[i].x - path[i].prevX == path[i + 1].x - path[i].x)
					&& (path[i].y - path[i].prevY == path[i + 1].y - path[i].y)
					) )
			{
				cout << "removing point " << path[i].x << ", " << path[i].y << endl;
				path[i].x = 0;
				path[i].y = 0;				
		}
				
			
			
			
			//cout << x1 << ", " << y1 << "    " << x2 << ", " << y2 << endl;
			//get y=mx+b line
			m = (y1*10-y2*10) / (x1*10-x2*10) ;
		
			//cout << "slope = " << m << endl;
			b = y1 - (m*x1);
			//cout << "ring ring now?" << endl;
			if (x1 <= x2) {
				for (int x = x1; x <= x2; x++) {
					if (y1 <= y2)
						for (int y = y1; y <= y2; y++) {
							/////////////check all these points for obstalces
					//		cout << "/////////////////////////////////////////////" << endl;
					//		cout << "line = " << y << "=" << m << "*" << x << "+" << b << "     so checking point " << x << ", " << y << endl;

						}
					else {
						for (int y = y2; y <= y1; y++) {
							/////////////check all these points for obstalces
						}
					
					}



				}
			}
			else{
				for (int x = x2; x <= x1; x++) {
					if (y1 <= y2)
						for (int y = y1; y <= y2; y++) {
							/////////////check all these points for obstalces

						}
					else {
						for (int y = y2; y <= y1; y++) {
							/////////////check all these points for obstalces
						}

					}

				}
			}
			//check each point on line. if no obstructions, set path[i-1].x(and .y) =0;
			//
			if (path[i].x != 0 && path[i].y != 0) {
				myQueue.push(path[i]);//actually add to queue
			}
		}
	}


};
